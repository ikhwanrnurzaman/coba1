<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="{{ asset('dota/img/favicon.png') }}" type="image/png">
	<title>Age of Empires - Strategic Battle Games</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{ asset('dota/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/linericon/style.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/owl-carousel/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/css/magnific-popup.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/nice-select/css/nice-select.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/animate-css/animate.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/flaticon/flaticon.css') }}">
	<!-- main css -->
	<link rel="stylesheet" href="{{ asset('dota/css/style.css') }}">
</head>

<body>

	<!--================Header Menu Area =================-->
	<header class="header_area">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h" href="index.html"><img src="/dota/img/logo.png" alt=""></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
						aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav justify-content-center">
								<li class="nav-item"><a class="nav-link" href="/aoe">Home</a></li>
								<li class="nav-item"><a class="nav-link" href="/aoe/ageofempire">Civilizations</a></li>
								<li class="nav-item"><a class="nav-link" href="/aoe/ageofempire2">Units</a></li>
								<li class="nav-item"><a class="nav-link" href="/aoe/ageofempire3">Structures</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</header>
	<!--================Header Menu Area =================-->

	<!--================Home Banner Area =================-->
	<section class="home_banner_area">
		<div class="banner_inner">
			<div class="container">
				<div class="row mt-5 pt-5">
					<div class="col-lg-6 mt-5 pt-5">
						<div class="home_left_img">
							<img class="img-fluid" src="/dota/img/banner/home-left.png" alt="">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="banner_content">
							<h2>
								Age <br>
								of <br>
								Emperors
							</h2>
							<p>
								Conquest every empire in the world! be the greatest empire in human history!
							</p>
							<div class="d-flex align-items-center">
								<a id="play-home-video" class="video-play-button" href="https://www.youtube.com/watch?v=3rd7zcKYZKU">
									<span></span>
								</a>
								<div class="watch_video text-uppercase">
									watch the video
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->

	<!--================Start About Us Area =================-->
	<section class="about_us_area section_gap_top">
		<div class="container">
			<div class="row about_content align-items-center">
				<div class="col-lg-6">
					<div class="section_content">
						<h6>About Age of Empires</h6>
						<h1>Classic RTS Game</h1>
						<p>Age of Empires is a series of historical real-time strategy video games, 
							originally developed by Ensemble Studios and published by Xbox Game Studios. The first title of the series was Age of Empires, released in 1997. 
							Seven titles and three spin-offs have been released</p>
						<a class="primary_btn" href="#">Learn More</a>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="about_us_image_box justify-content-center">
						<img class="img-fluid w-100" src="https://steamcdn-a.akamaihd.net/steam/apps/105450/ss_bbc0841c5991b51395453c59e076b854d669d779.1920x1080.jpg?t=1547569230" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End About Us Area =================-->

	<!--================ Start Gallery Area =================-->
	<section class="gallery_area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="main_title text-white">
						<h2>Screens Gallery</h2>
						<h1 style="color: white;">Screens Gallery</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<div class="single-gallery">
								<div class="overlay"></div>
								<img class="img-fluid w-100" src="https://images8.alphacoders.com/532/thumb-1920-532305.jpg" alt="">
								<div class="content">
									<a class="pop-up-image" href="https://images8.alphacoders.com/532/thumb-1920-532305.jpg">
										<i class="lnr lnr-eye"></i>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6">
							<div class="single-gallery">
								<div class="overlay"></div>
								<img class="img-fluid w-100" src="https://wallpapercave.com/wp/wp1826580.jpg" alt="">
								<div class="content">
									<a class="pop-up-image" href="https://wallpapercave.com/wp/wp1826580.jpg">
										<i class="lnr lnr-eye"></i>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="single-gallery">
								<div class="overlay"></div>
								<img class="img-fluid w-100" src="https://wallpapercave.com/wp/wp1826560.jpg" alt="">
								<div class="content">
									<a class="pop-up-image" href="https://wallpapercave.com/wp/wp1826560.jpg">
										<i class="lnr lnr-eye"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 hidden-md hidden-sm">
					<div class="single-gallery">
						<div class="overlay"></div>
						<img class="img-fluid w-100" src="https://www.desktop-background.com/download/480x800/2015/07/12/978326_age-of-empires-ii-hd-wallpapers_1920x1080_h.jpg" alt="">
						<div class="content">
							<a class="pop-up-image" href="https://www.desktop-background.com/download/480x800/2015/07/12/978326_age-of-empires-ii-hd-wallpapers_1920x1080_h.jpg">
								<i class="lnr lnr-eye"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================ End Gallery Area =================-->

	<!--================ Start Blog Area ================-->
	{{-- <section class="blog_area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="main_title">
						<h2>Latest Blog Posts</h2>
						<h1 style="color: white;">Latest Blog Posts</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<div class="blog_items">
						<div class="blog_img_box">
							<img class="img-fluid" src="dota/img/blog_img1.png" alt="">
						</div>
						<div class="blog_content">
							<a class="title" href="blog.html">Portable Fashion for women</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
								incididunt ut labore et
								dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
							<div class="date">
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="blog_items">
						<div class="blog_img_box">
							<img class="img-fluid" src="dota/img/blog_img2.png" alt="">
						</div>
						<div class="blog_content">
							<a class="title" href="blog.html">Portable Fashion for women</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
								incididunt ut labore et
								dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
							<div class="date">
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 hidden-md">
					<div class="blog_items">
						<div class="blog_img_box">
							<img class="img-fluid" src="dota/img/blog_img3.png" alt="">
						</div>
						<div class="blog_content">
							<a class="title" href="blog.html">Portable Fashion for women</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
								incididunt ut labore et
								dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</section> --}}
	<!--================ End Blog Area ================-->

	<!--================Footer Area =================-->
	<footer class="footer_area">
		<div class="container">
			<div class="row single-footer-widget">
				<div class="col-md">
					<div class="copy_right_text">
						<p>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							Copyright &copy;<script>
								document.write(new Date().getFullYear());
							</script> All rights reserved | This template is made with <i class="fa fa-heart-o"
								aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--================End Footer Area =================-->

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="{{ asset('dota/js/jquery-3.2.1.min.js') }}"></script>
	<script src="{{ asset('dota/js/popper.js') }}"></script>
	<script src="{{ asset('dota/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('dota/js/stellar.js') }}"></script>
	<script src="{{ asset('dota/js/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/nice-select/js/jquery.nice-select.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/isotope/imagesloaded.pkgd.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/isotope/isotope-min.js') }}"></script>
	<script src="{{ asset('dota/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('dota/js/jquery.ajaxchimp.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/counter-up/jquery.waypoints.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/counter-up/jquery.counterup.min.js') }}"></script>
	<script src="{{ asset('dota/js/mail-script.js') }}"></script>
	<!--gmaps Js-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="{{ asset('dota/js/gmaps.min.js') }}"></script>
	<script src="{{ asset('dota/js/theme.js') }}"></script>
</body>

</html>