@extends('dota.layouts.master')

@section('content')
<!--================Header Menu Area =================-->
<header class="header_area">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <h1 class="navbar-brand logo_h text-white">Age of Empires II</h1>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav justify-content-center">
                        <li class="nav-item"><a class="nav-link" href="/aoe">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="/aoe/ageofempire">Civilizations</a></li>
                        <li class="nav-item"><a class="nav-link" href="/aoe/ageofempire2">Units</a></li>
                        <li class="nav-item"><a class="nav-link" href="/aoe/ageofempire3">Structures</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="nav-item"><a href="{{ URL::previous() }}" class="primary_btn">BACK</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<!--================Header Menu Area =================-->
<!--================Home Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0"
            data-background=""></div>
        <div class="container">
            <div class="banner_content text-center">
                <h2>Structures Detail</h2>
            </div>
        </div>
    </div>
</section>
<!--================End Home Banner Area =================-->
<!--Isi Konten disini-->

<div class="text-white">
<h3>Structure Detail id: {{ $detailstructure->id }}</h3>
<br>

<p>id: {{ $detailstructure->id }}</p>
<p>Name: {{ $detailstructure->name }}</p>
<p>Expansion: {{ $detailstructure->expansion }}</p>
<p>Age: {{ $detailstructure->age }}</p>
	<p>Build Time: {{ $detailstructure->build_time }}</p>
	<p>Hit Points: {{ $detailstructure->hit_points }}</p>
	<p>Armor: {{ $detailstructure->armor }}</p>
	<p>Special: @foreach($detailstructure->special as $detailspecial)
							<span rowspan="1">{{$detailspecial}}</span><br>	
						@endforeach</p>
</div>

@endsection