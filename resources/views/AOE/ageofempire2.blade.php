@extends('dota.layouts.master')

@section('content')
<!--================Header Menu Area =================-->
<header class="header_area">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <h1 class="navbar-brand logo_h text-white">Age of Empires II</h1>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav justify-content-center">
                        <li class="nav-item"><a class="nav-link" href="/aoe">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="/aoe/ageofempire">Civilizations</a></li>
                        <li class="nav-item active"><a class="nav-link" href="/aoe/ageofempire2">Units</a></li>
                        <li class="nav-item"><a class="nav-link" href="/aoe/ageofempire3">Structures</a></li>
                    </ul>
                 </div>
            </div>
        </nav>
    </div>
</header>
<!--================Header Menu Area =================-->
<!--================Home Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0"
            data-background=""></div>
        <div class="container">
            <div class="banner_content text-center">
                <h2>Units</h2>
            </div>
        </div>
    </div>
</section>
<!--================End Home Banner Area =================-->


<div class="container text-white">
	<div class="row justify-content-center">
			<table class="table table-hover" style="width: 100%">
				<tr>
					<div class="col-md-2"><th scope="col">Name</th></div>
					<div class="col-md-6"><th scope="col">Description</th></div>
					<div class="col-md-3"><th scope="col">Expansion</th></div>
					<div class="col-md-2"><th scope="col">Age</th></div>
				</tr>
				@foreach($units->units as $pecah2)
				<tr>
					<td class="align-middle">{{ $pecah2->name }}</td>
					<td class="align-middle">{{ $pecah2->description }}</td>
					<td class="align-middle">{{ $pecah2->expansion }}</td>
					<td class="align-middle">{{ $pecah2->age }}</td>
				</tr>
				@endforeach


			</table>
	
	
	</div>
	

</div>


@endsection
