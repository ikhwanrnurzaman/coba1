<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>GameMania - Your Favorite Games Portal</title>

<!-- Bootstrap core CSS -->
<link href="{{ asset('/portal/vendor/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

<!-- Custom fonts for this template -->
<link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i"
    rel="stylesheet">

<!-- Custom styles for this template -->
<link href="{{ asset('/portal/css/one-page-wonder.css') }}" rel="stylesheet">

</head>

<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">GameMania</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="#">Sign Up</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Log In</a>
            </li>
        </ul>
        </div>
    </div>
</nav>

<header class="masthead text-center">
    <div class="masthead-content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col">
                    <div class="card bg-dark text-white" style="width: 18rem;">
                        <img class="card-img-top" src="https://cdn-images-1.medium.com/max/1200/0*vbw4wQW_Xq2_3eOo.jpg"
                    alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">DoTA 2</h5>
                            <p class="card-text">Eternal battle between Radiant and Dire</p>
                            <a href="/dota2" target="_blank" class="btn btn-primary">Defend The Ancient</a>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card bg-dark text-white" style="width: 18rem;">
                        <img class="card-img-top"
                        src="https://cdn.vox-cdn.com/uploads/chorus_image/image/63673188/Fortnite_Avengers_Endgame.0.jpg"
                        alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Fortnite</h5>
                            <p class="card-text">Kill to be number One</p>
                            <a href="/fortnite" target="_blank" class="btn btn-primary">Royale Battle</a>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card bg-dark text-white" style="width: 18rem;">
                        <img class="card-img-top" src="https://images4.alphacoders.com/568/thumb-1920-568033.png"
                        alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Age of Empires II</h5>
                            <p class="card-text">Be a Great Empire, Conquest The World!</p>
                            <a href="/aoe" target="_blank" class="btn btn-primary">Attack!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<section>
    <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 order-lg-2">
                    <div class="p-5">
                        <img class="img-fluid rounded-circle" src="dota/img/1.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-6 order-lg-1">
                    <div class="p-5">
                        <h2 class="display-4">Nevermore, <br> The Shadow Fiend</h2>
                        <p>I sense the proximity of souls...</p>
                    </div>
                </div>
            </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="p-5">
                    <img class="img-fluid rounded-circle" src="dota/img/2.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="p-5">
                    <h2 class="display-4">Raven</h2>
                    <p>Night is stealthy, Evil raven, wrapt to the eyes in his black wings</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 order-lg-2">
                <div class="p-5">
                    <img class="img-fluid rounded-circle" src="dota/img/3.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6 order-lg-1">
                <div class="p-5">
                    <h2 class="display-4">Gilgamesh, <br> The King of Kings</h2>
                    <p>The things we can't get are the most beautiful ones.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<footer class="py-5 bg-black">
    <div class="container">
        <p class="m-0 text-center text-white small">Copyright &copy; Your Website 2019</p>
    </div>
    <!-- /.container -->
</footer>
<!-- Bootstrap core JavaScript -->
<script src="{{ asset('/portal/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('/portal/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

</body>

</html>