<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="{{ asset('dota/img/favicon.png') }}" type="image/png">
	<title>Dota - Defends of the Ancient</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{ asset('dota/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/linericon/style.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/owl-carousel/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/css/magnific-popup.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/nice-select/css/nice-select.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/animate-css/animate.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/flaticon/flaticon.css') }}">
	<!-- main css -->
	<link rel="stylesheet" href="{{ asset('dota/css/style.css') }}">
</head>

<body>

	<!--================Header Menu Area =================-->
	<header class="header_area">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h" href="index.html"><img src="/dota/img/logo.png" alt=""></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
						aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav justify-content-center">
							<li class="nav-item active"><a class="nav-link" href="/dota2">Home</a></li>
							<li class="nav-item"><a class="nav-link" href="/dota2/team">Team</a></li>
							<li class="nav-item"><a class="nav-link" href="/dota2/hero">Heroes</a>
							<li class="nav-item"><a class="nav-link" href="/dota2/article">Blog</a>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</header>
	<!--================Header Menu Area =================-->

	<!--================Home Banner Area =================-->
	<section class="home_banner_area">
		<div class="banner_inner">
			<div class="container">
				<div class="row mt-5 pt-5">
					<div class="col-lg-6 mt-5 pt-5">
						<div class="home_left_img">
							<img class="img-fluid" src="/dota/img/banner/home-left.png" alt="">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="banner_content">
							<h2>
								Defends <br>
								of <br>
								The Ancient
							</h2>
							<p>
								Endless battle between Radiant and Dire. Protecting their precious. The tree of life and The hell fire.
							</p>
							<div class="d-flex align-items-center">
								<a id="play-home-video" class="video-play-button" href="https://www.youtube.com/watch?v=bUM8TDZ_Ey8">
									<span></span>
								</a>
								<div class="watch_video text-uppercase">
									watch the video
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->

	<!--================Start About Us Area =================-->
	<section class="about_us_area section_gap_top">
		<div class="container">
			<div class="row about_content align-items-center">
				<div class="col-lg-6">
					<div class="section_content">
						<h6>About Dota 2</h6>
						<h1>World's Best MOBA PC Game</h1>
						<p>DoTA 2 is a multiplayer online battle arena (MOBA) developed and published by Valve Corporation.
							The game is a sequel to Defense of the Ancient, which was community-created mod for Blizzard Entertainment's Warcraft III:Reign of Chaos,
							and its expansion pack, The Frozen Throne.
						</p>
						<a class="primary_btn" href="https://www.dota2.com">Learn More</a>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="about_us_image_box justify-content-center">
						<img class="img-fluid w-100" src="http://i.imgur.com/bJsm402.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End About Us Area =================-->

	<!--================ Start Gallery Area =================-->
	<section class="gallery_area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="main_title text-white">
						<h2>Screens Gallery</h2>
						<h1 style="color: white;">Screens Gallery</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<div class="single-gallery">
								<div class="overlay"></div>
								<img class="img-fluid w-100" src="http://s1.picswalls.com/wallpapers/2014/09/18/awesome-dota-2-wallpaper_112428151_222.jpg" alt="">
								<div class="content">
									<a class="pop-up-image" href="http://s1.picswalls.com/wallpapers/2014/09/18/awesome-dota-2-wallpaper_112428151_222.jpg">
										<i class="lnr lnr-eye"></i>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6">
							<div class="single-gallery">
								<div class="overlay"></div>
								<img class="img-fluid w-100" src="https://i0.wp.com/wallur.com/wp-content/uploads/2016/12/dota-background-6.jpg?fit=1920%2C1080" alt="">
								<div class="content">
									<a class="pop-up-image" href="https://i0.wp.com/wallur.com/wp-content/uploads/2016/12/dota-background-6.jpg?fit=1920%2C1080">
										<i class="lnr lnr-eye"></i>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="single-gallery">
								<div class="overlay"></div>
								<img class="img-fluid w-100" src="https://i.imgur.com/qRxyu3L.jpg" alt="">
								<div class="content">
									<a class="pop-up-image" href="https://i.imgur.com/qRxyu3L.jpg">
										<i class="lnr lnr-eye"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 hidden-md hidden-sm">
					<div class="single-gallery">
						<div class="overlay"></div>
						<img class="img-fluid w-100" src="http://www.cerc-ug.org/pic/b/63/638761_dota-2-arcana-wallpaper.jpg" alt="">
						<div class="content">
							<a class="pop-up-image" href="http://www.cerc-ug.org/pic/b/63/638761_dota-2-arcana-wallpaper.jpg">
								<i class="lnr lnr-eye"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================ End Gallery Area =================-->

	<!--================ Start Blog Area ================-->
	<section class="blog_area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="main_title">
						<h2>Latest Blog Posts</h2>
						<h1 style="color: white;">Latest Blog Posts</h1>
					</div>
				</div>
			</div>
			<div class="row">
				@foreach ($all_article as $item)
				@if ($loop->iteration>3)
					@break
				@endif
					<div class="col-lg-4 col-md-6">
						<a href="/dota2/{{$item->id}}/edit">
							<div class="blog_items">
								<div class="blog_img_box">
									<img class="img-fluid" src="{{$item->img_link}}" alt="">
								</div>
								<div class="blog_content">
									<a class="title" href="blog.html">{{$item->title}}</a>
									<p>{{$item->content}}</p>
									<div class="date">
									</div>
								</div>
							</div>
						</a>
					</div>
				@endforeach
			</div>
		</div>
		</div>
	</section>
	<!--================ End Blog Area ================-->

	<!--================Footer Area =================-->
	<footer class="footer_area">
		<div class="container">
			<div class="row single-footer-widget">
				<div class="col-md">
					<div class="copy_right_text">
						<p>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							Copyright &copy;<script>
								document.write(new Date().getFullYear());
							</script> All rights reserved | This template is made with <i class="fa fa-heart-o"
								aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--================End Footer Area =================-->

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="{{ asset('dota/js/jquery-3.2.1.min.js') }}"></script>
	<script src="{{ asset('dota/js/popper.js') }}"></script>
	<script src="{{ asset('dota/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('dota/js/stellar.js') }}"></script>
	<script src="{{ asset('dota/js/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/nice-select/js/jquery.nice-select.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/isotope/imagesloaded.pkgd.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/isotope/isotope-min.js') }}"></script>
	<script src="{{ asset('dota/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('dota/js/jquery.ajaxchimp.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/counter-up/jquery.waypoints.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/counter-up/jquery.counterup.min.js') }}"></script>
	<script src="{{ asset('dota/js/mail-script.js') }}"></script>
	<!--gmaps Js-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="{{ asset('dota/js/gmaps.min.js') }}"></script>
	<script src="{{ asset('dota/js/theme.js') }}"></script>
</body>

</html>