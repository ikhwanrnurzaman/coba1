@extends('dota.layouts.master')

@section('content')
<!--================Header Menu Area =================-->
<header class="header_area">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <h1 class="navbar-brand logo_h text-white">DoTA 2</h1>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav justify-content-center">
                        <li class="nav-item"><a class="nav-link" href="/dota2">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="/dota2/team">Team</a></li>
                        <li class="nav-item"><a class="nav-link" href="/dota2/hero">Heroes</a>
                        <li class="nav-item active"><a class="nav-link" href="/dota2/article">Article</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<!--================Header Menu Area =================-->
<!--================Home Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0"
            data-background=""></div>
        <div class="container">
            <div class="banner_content text-center">
                <h2>Dota 2 Blog</h2>
            </div>
        </div>
    </div>
</section>
<!--================End Home Banner Area =================-->

<!--Isi Konten disini-->
<div class="container my-auto">
    <form action="/dota2/{{$artikel->id}}" method="POST">
        <input type="text" name="title" class="form-control m-1" placeholder="{{$artikel->title}}"><br>
        <textarea name="content" id="" cols="30" rows="10" class="form-control m-2">{{$artikel->content}}</textarea><br>
        <input type="text" name="img_link" class="form-control m-2" placeholder="{{$artikel->img_link}}"><br>
        <input type="submit" name="submit" value="Update" class="btn btn-primary m-2 center">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
    </form>
</div>
@endsection