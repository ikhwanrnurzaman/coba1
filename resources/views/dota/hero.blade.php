@extends('dota.layouts.master')

@section('content')
<!--================Header Menu Area =================-->
<header class="header_area">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <h1 class="navbar-brand logo_h text-white">DoTA 2</h1>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav justify-content-center">
                        <li class="nav-item"><a class="nav-link" href="/dota2">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="/dota2/team">Team</a></li>
                        <li class="nav-item active"><a class="nav-link" href="/dota2/hero">Heroes</a>
                        <li class="nav-item"><a class="nav-link" href="/dota2/article">Blog</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<!--================Header Menu Area =================-->
<!--================Home Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0"
            data-background=""></div>
        <div class="container">
            <div class="banner_content text-center">
                <h2>Dota 2 Heroes</h2>
            </div>
        </div>
    </div>
</section>
<!--================End Home Banner Area =================-->

<!--Isi Konten disini-->
<div class="container my-auto text-white">
    <div class="row">
    @foreach ($Heros as $item)
        @if ($loop->iteration>12)
            @break
        @endif
        <div class="col col-md-4 my-4">
            <div class="card text-center" style="width: 18rem;">
                <img class="card-img-top" style='height:144px' src="https://api.opendota.com{{ $item->img }}"
                alt="Card image cap">
                <div class="card-body">
                    <h2 class="card-title">{{ $item->localized_name }}</h3>
                    <h4 class="card-text">Attack Type</h4>
                    <p class="card-text"> {{ $item->attack_type}} </p><hr>
                    <h4 class="card-text">Roles</h4>
                    <p class="card-text">
                        @foreach ($item->roles as $key)
                            @if ($loop->last)
                                {{ $key }}
                            @else
                                {{ $key }} |
                            @endif
                        @endforeach
                    </p>
                </div>
            </div>
        </div>
    @endforeach
    </div>
</div>
@endsection