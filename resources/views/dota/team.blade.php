@extends('dota.layouts.master')

@section('content')
<!--================Header Menu Area =================-->
<header class="header_area">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <h1 class="navbar-brand logo_h text-white">DoTA 2</h1>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav justify-content-center">
                        <li class="nav-item"><a class="nav-link" href="/dota2">Home</a></li>
                        <li class="nav-item active"><a class="nav-link" href="/dota2/team">Team</a></li>
                        <li class="nav-item"><a class="nav-link" href="/dota2/hero">Heroes</a>
                        <li class="nav-item"><a class="nav-link" href="/dota2/article">Blog</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<!--================Header Menu Area =================-->
<!--================Home Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0"
            data-background=""></div>
        <div class="container">
            <div class="banner_content text-center">
                <h2>Team</h2>
            </div>
        </div>
    </div>
</section>
<!--================End Home Banner Area =================-->

<!--Isi Konten disini-->
<div class="container text-white my-auto">
    <div class="row">
        <div class="col-md-12">
            <div class="row justify-content-center my-2">
                <img src="{{$secretTeam->logo_url}}" alt="">
            </div>
            <div class="row justify-content-center my-2">
                <h1>{{ $secretTeam->name }}</h1><br>
            </div>
            <div class="row justify-content-center">
                <table class="table text-center">
                    <tr>
                        <th>Name</th>
                        <th>Game Played</th>
                        <th>Wins</th>
                    </tr>
                    @foreach ($secretPlayer as $item)
                        @if ($item->is_current_team_member)
                            <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->games_played }}</td>
                                    <td>{{ $item->wins }}</td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row justify-content-center my-2">
                <img src="{{$OGTeam->logo_url}}" alt="">
            </div>
            <div class="row justify-content-center my-2">
                <h1>{{ $OGTeam->name }}</h1><br>
            </div>
            <div class="row justify-content-center">
                <table class="table text-center">
                    <tr>
                        <th>Name</th>
                        <th>Game Played</th>
                        <th>Wins</th>
                    </tr>
                    @foreach ($OGPlayer as $item)
                        @if ($item->is_current_team_member)
                            <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->games_played }}</td>
                                    <td>{{ $item->wins }}</td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row justify-content-center my-2">
                <img src="{{$LiquidTeam->logo_url}}" alt="">
            </div>
            <div class="row justify-content-center my-2">
                <h1>{{ $LiquidTeam->name }}</h1><br>
            </div>
            <div class="row justify-content-center">
                <table class="table text-center">
                    <tr>
                        <th>Name</th>
                        <th>Game Played</th>
                        <th>Wins</th>
                    </tr>
                    @foreach ($LiquidPlayer as $item)
                        @if ($item->is_current_team_member)
                            <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->games_played }}</td>
                                    <td>{{ $item->wins }}</td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection