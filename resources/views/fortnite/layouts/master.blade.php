<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="img/favicon.png" type="image/png">
	<title>Nexus SaaS</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{ asset('dota/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/linericon/style.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/owl-carousel/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/css/magnific-popup.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/nice-select/css/nice-select.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/animate-css/animate.css') }}">
	<!-- main css -->
	<link rel="stylesheet" href="{{ asset('dota/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/css/responsive.css') }}">
</head>

<body class="gallery_page">

	{{-- <!--================Header Menu Area =================-->
	<header class="header_area">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h" href="index.html"><img src="img/logo.png" alt=""></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
						aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav justify-content-center">
							<li class="nav-item"><a class="nav-link" href="index.html">Home</a></li>
							<li class="nav-item"><a class="nav-link" href="about-us.html">About</a></li>
							<li class="nav-item active"><a class="nav-link" href="gallery.html">Gallery</a>
							<li class="nav-item submenu dropdown">
								<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
									aria-haspopup="true" aria-expanded="false">Pages</a>
								<ul class="dropdown-menu">
									<li class="nav-item"><a class="nav-link" href="price.html">Pricing</a>
									<li class="nav-item"><a class="nav-link" href="games.html">Games</a>
									<li class="nav-item"><a class="nav-link" href="elements.html">Elements</a>
								</ul>
							</li>
							<li class="nav-item submenu dropdown">
								<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
									aria-haspopup="true" aria-expanded="false">Blog</a>
								<ul class="dropdown-menu">
									<li class="nav-item"><a class="nav-link" href="blog.html">Blog</a></li>
									<li class="nav-item"><a class="nav-link" href="single-blog.html">Blog Details</a>
									</li>
								</ul>
							</li>
							<li class="nav-item"><a class="nav-link" href="contact.html">Contact</a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="nav-item"><a href="#" class="primary_btn">join us</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</header>
	<!--================Header Menu Area =================--> --}}
	@yield('content')
	<!--================Footer Area =================-->
	<footer class="footer_area">
		<div class="container">
			<div class="row single-footer-widget">
				<div class="col-lg-6 col-md-6 col-sm-12">
					<div class="copy_right_text">
						<p>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							Copyright &copy;<script>
								document.write(new Date().getFullYear());
							</script> All rights reserved | This template is made with <i class="fa fa-heart-o"
								aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--================End Footer Area =================-->

	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	@include('sweet::alert')

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="{{ asset('dota/js/jquery-3.2.1.min.js') }}"></script>
	<script src="{{ asset('dota/js/popper.js') }}"></script>
	<script src="{{ asset('dota/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('dota/js/stellar.js') }}"></script>
	<script src="{{ asset('dota/js/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/nice-select/js/jquery.nice-select.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/isotope/imagesloaded.pkgd.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/isotope/isotope-min.js') }}"></script>
	<script src="{{ asset('dota/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('dota/js/jquery.ajaxchimp.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/counter-up/jquery.waypoints.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/counter-up/jquery.counterup.min.js') }}"></script>
	<script src="{{ asset('dota/js/mail-script.js') }}"></script>
	<!--gmaps Js-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="{{ asset('dota/js/gmaps.min.js') }}"></script>
	<script src="{{ asset('dota/js/theme.js') }}"></script>
</body>

</html>