@extends('dota.layouts.master')

@section('content')
<!--================Header Menu Area =================-->
<header class="header_area">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <h1 class="navbar-brand logo_h text-white">Fortnite</h1>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav justify-content-center">
                        <li class="nav-item"><a class="nav-link" href="/fortnite">HOME</a></li>
                        <li class="nav-item"><a class="nav-link" href="/fortnite/allitem">ALL ITEM</a></li>
                        <li class="nav-item"><a class="nav-link" href="/fortnite/weapon">WEAPON</a>
                        <li class="nav-item active"><a class="nav-link" href="/fortnite/news">NEWS</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<!--================Header Menu Area =================-->
<!--================Home Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0"
            data-background=""></div>
        <div class="container">
            <div class="banner_content text-center">
                <h2>Fortnite News</h2>
            </div>
        </div>
    </div>
</section>
<!--================End Home Banner Area =================-->

<!--Isi Konten disini-->
  <div class="container my-auto text-white">
    <div class="row">
        @foreach ($entries as $item)
        @if ($loop->iteration>31)
        @break
        
        @endif

   <div class="col-md-4">
        <div class="card mb-4"  style="width: 21rem;">
                
                <img class="card-img-top p-2" src="{{$item->image}}" alt="Card image cap">
                <div class="card-body text-center">
                    <h2 class="card-text">{{$item->title}} </h2>
                    <p class="card-text">{{$item->body}} </p>
                </div>
        </div>
    </div>


    @endforeach
  </div>
  </div>
          @endsection