<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="{{ asset('dota/img/favicon.png') }}" type="image/png">
	<title>Fortnite - Battle Royale</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{ asset('dota/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/linericon/style.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/owl-carousel/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/css/magnific-popup.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/nice-select/css/nice-select.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/animate-css/animate.css') }}">
	<link rel="stylesheet" href="{{ asset('dota/vendors/flaticon/flaticon.css') }}">
	<!-- main css -->
	<link rel="stylesheet" href="{{ asset('dota/css/style.css') }}">
</head>

<body>

	<!--================Header Menu Area =================-->
	<header class="header_area">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h" href="index.html"><img src="/dota/img/logo.png" alt=""></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
						aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav justify-content-center">
							<li class="nav-item active"><a class="nav-link" href="fortnite">Home</a></li>
							<li class="nav-item"><a class="nav-link" href="fortnite/weapon">Weapon</a></li>
							<li class="nav-item"><a class="nav-link" href="fortnite/news">News</a></li>
							<li class="nav-item"><a class="nav-link" href="fortnite/allitem">All Item</a></li>								
						</ul>									
					</div>
				</div>
			</nav>
		</div>
	</header>
	<!--================Header Menu Area =================-->

	<!--================Home Banner Area =================-->
	<section class="home_banner_area">
		<div class="banner_inner">
			<div class="container">
				<div class="row mt-5 pt-5">
					<div class="col-lg-6 mt-5 pt-5">
						<div class="home_left_img">
							<img class="img-fluid" src="dota/img/banner/home-left.png" alt="">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="banner_content">
							<h2>
								Fortnite <br>
								Battle Royale<br>
							</h2>
							<p>
								Royale tournament between up to 100 agents. Kill as much as you can. Survive till the end of tournament. Be the greatest agent in the World's.
							</p>
							<div class="d-flex align-items-center">
								<a id="play-home-video" class="video-play-button" href="https://www.youtube.com/watch?v=RvkyWFPndOU">
									<span></span>
								</a>
								<div class="watch_video text-uppercase">
									watch the video
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->

	<!--================Start About Us Area =================-->
	<section class="about_us_area section_gap_top">
		<div class="container">
			<div class="row about_content align-items-center">
				<div class="col-lg-6">
					<div class="section_content">
						<h6>About Fortnite</h6>
						<h1>World's Best Battle Royale Game</h1>
						<p>Fortnite is an online video game developed by Epic Games and released in 2017. It is available in three distinct game mode versions that otherwise share the same general gameplay and game engine: 
							Fortnite: Save the World, a cooperative shooter-survival game for up to four players to fight off zombie-like creatures and defend objects with fortifications they can build, 
							Fortnite Battle Royale, a free-to-play battle royale game where up to 100 players fight to be the last person standing, 
							and Fortnite Creative, where players are given complete freedom to create worlds and battle arenas.</p>
						<a class="primary_btn" href="https://www.epicgames.com/fortnite/en-US/home" target="_blank">Learn More</a>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="about_us_image_box justify-content-center">
						<img class="img-fluid w-100" src="https://images8.alphacoders.com/877/thumb-1920-877849.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End About Us Area =================-->

	<!--================ Start Gallery Area =================-->
	<section class="gallery_area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="main_title">
						<h2>Screens Gallery</h2>
						<h1 style="color: white;">Screens Gallery</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<div class="single-gallery">
								<div class="overlay"></div>
								<img class="img-fluid w-100" src="http://hdqwalls.com/wallpapers/fortnite-battle-royale-abstrakt-skin-ov.jpg	" alt="">
								<div class="content">
									<a class="pop-up-image" href="http://hdqwalls.com/wallpapers/fortnite-battle-royale-abstrakt-skin-ov.jpg	">
										<i class="lnr lnr-eye"></i>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6">
							<div class="single-gallery">
								<div class="overlay"></div>
								<img class="img-fluid w-100" src="https://assets1.ignimgs.com/thumbs/userUploaded/2019/2/28/fortniteseason8trailerblogroll-1551353328905.jpg" alt="">
								<div class="content">
									<a class="pop-up-image" href="https://assets1.ignimgs.com/thumbs/userUploaded/2019/2/28/fortniteseason8trailerblogroll-1551353328905.jpg">
										<i class="lnr lnr-eye"></i>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="single-gallery">
								<div class="overlay"></div>
								<img class="img-fluid w-100" src="https://wallpapercave.com/wp/wp3538790.jpg" alt="">
								<div class="content">
									<a class="pop-up-image" href="https://wallpapercave.com/wp/wp3538790.jpg">
										<i class="lnr lnr-eye"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 hidden-md hidden-sm">
					<div class="single-gallery">
						<div class="overlay"></div>
						<img class="img-fluid w-100" src="https://i.pinimg.com/originals/33/0c/42/330c424f4045aa91b8893043ce7dacaa.jpg" alt="">
						<div class="content">
							<a class="pop-up-image" href="https://i.pinimg.com/originals/33/0c/42/330c424f4045aa91b8893043ce7dacaa.jpg">
								<i class="lnr lnr-eye"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================ End Gallery Area =================-->

	<!--================ Start Blog Area ================-->
	{{-- <section class="blog_area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="main_title">
						<h2>Latest Blog Posts</h2>
						<h1 style="color: white;">Latest Blog Posts</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<div class="blog_items">
						<div class="blog_img_box">
							<img class="img-fluid" src="dota/img/blog_img1.png" alt="">
						</div>
						<div class="blog_content">
							<a class="title" href="blog.html">Portable Fashion for women</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
								incididunt ut labore et
								dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
							<div class="date">
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="blog_items">
						<div class="blog_img_box">
							<img class="img-fluid" src="dota/img/blog_img2.png" alt="">
						</div>
						<div class="blog_content">
							<a class="title" href="blog.html">Portable Fashion for women</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
								incididunt ut labore et
								dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
							<div class="date">
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 hidden-md">
					<div class="blog_items">
						<div class="blog_img_box">
							<img class="img-fluid" src="dota/img/blog_img3.png" alt="">
						</div>
						<div class="blog_content">
							<a class="title" href="blog.html">Portable Fashion for women</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
								incididunt ut labore et
								dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</section> --}}
	<!--================ End Blog Area ================-->

	<!--================Footer Area =================-->
	<footer class="footer_area">
		<div class="container">
			<div class="row single-footer-widget">
				<div class="col-md">
					<div class="copy_right_text">
						<p>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							Copyright &copy;<script>
								document.write(new Date().getFullYear());
							</script> All rights reserved | This template is made with <i class="fa fa-heart-o"
								aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--================End Footer Area =================-->

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="{{ asset('dota/js/jquery-3.2.1.min.js') }}"></script>
	<script src="{{ asset('dota/js/popper.js') }}"></script>
	<script src="{{ asset('dota/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('dota/js/stellar.js') }}"></script>
	<script src="{{ asset('dota/js/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/nice-select/js/jquery.nice-select.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/isotope/imagesloaded.pkgd.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/isotope/isotope-min.js') }}"></script>
	<script src="{{ asset('dota/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('dota/js/jquery.ajaxchimp.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/counter-up/jquery.waypoints.min.js') }}"></script>
	<script src="{{ asset('dota/vendors/counter-up/jquery.counterup.min.js') }}"></script>
	<script src="{{ asset('dota/js/mail-script.js') }}"></script>
	<!--gmaps Js-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="{{ asset('dota/js/gmaps.min.js') }}"></script>
	<script src="{{ asset('dota/js/theme.js') }}"></script>
</body>

</html>