<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Landing Pages
Route::get('/gamemania', 'HomeController@home');

//Dota Routes
Route::get('/dota2', 'DotaController@dota');

Route::get('/dota2/team', 'DotaController@team');

Route::get('/dota2/hero', 'DotaController@heros');

Route::get('/dota2/article', 'ArticleController@create');

Route::post('/dota2', 'ArticleController@store');

Route::get('/dota2/{id}/edit', 'ArticleController@edit');

Route::put('/dota2/{id}', 'ArticleController@update');

//Fortnite Routes
Route::get('/fortnite','FortniteController@showfortnite');

Route::get('/fortnite/allitem','FortniteController@allitem');

Route::get('/fortnite/weapon','FortniteController@weapon');

Route::get('fortnite/news','FortniteController@news');

//AOE2 Routes
Route::get('/aoe', 'AOEController@aoe');

Route::get('/aoe/ageofempire' , 'AOEController@ageofempire');

Route::get('/aoe/ageofempire2' , 'AOEController@ageofempire2');

Route::get('/aoe/ageofempire3' , 'AOEController@ageofempire3');

Route::get('/aoe/detail-structure/{id}' , 'AOEController@detail_structure');

//Authentication
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
