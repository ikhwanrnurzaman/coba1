<?php

namespace App\Http\Controllers;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Article; //model yang kita buat kita panggil


class AOEController extends Controller
{
    public function ageofempire(){
    	$client = new Client();
    	$uri = 'https://age-of-empires-2-api.herokuapp.com/api/v1/civilizations';
    	$response = $client->get($uri);
    	$civilization = json_decode($response->getBody()->getContents());
    	return view('AOE.ageofempire', compact('civilization'));
    }


    public function ageofempire2(){
    	$client = new Client();
    	$uri = 'https://age-of-empires-2-api.herokuapp.com/api/v1/units';
    	$response = $client->get($uri);
    	$units = json_decode($response->getBody()->getContents());
    	return view('AOE.ageofempire2', compact('units'));
    }

    public function ageofempire3(){
    	$client = new Client();
    	$uri = 'https://age-of-empires-2-api.herokuapp.com/api/v1/structures';
    	$response = $client->get($uri);
    	$structure = json_decode($response->getBody()->getContents())->structures;
    	//dd($structure);
    	return view('AOE.ageofempire3', compact('structure'));
    
	}
    public function detail_structure($id){
    	$client = new Client();
    	$uri = 'https://age-of-empires-2-api.herokuapp.com/api/v1/structure/'.$id;
    	$response = $client->get($uri);
    	$detailstructure = json_decode($response->getBody()->getContents());
    	return view('AOE.detail-structure', compact('detailstructure'));
	}
	
	public function aoe(){
		return view('AOE.aoe');
	}
}
