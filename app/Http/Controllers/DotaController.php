<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Article;

class DotaController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function dota(){
        $all_article = Article::all();

        return view('dota.dota', compact('all_article'));
    }

    public function team(){
        //Secret Team
        $clientPlayerSecret = new Client();
        $urlPlayerSecret = 'https://api.opendota.com/api/teams/1838315/players';
        $responsePlayer = $clientPlayerSecret->get($urlPlayerSecret);
        $secretPlayer = json_decode($responsePlayer->getBody()->getContents());
        $clientTeamSecret = new Client();
        $urlTeamSecret = 'https://api.opendota.com/api/teams/1838315';
        $responseTeam = $clientTeamSecret->get($urlTeamSecret);
        $secretTeam = json_decode($responseTeam->getBody()->getContents());

        //OG
        $clientPlayerOG = new Client();
        $urlPlayerOG = 'https://api.opendota.com/api/teams/2586976/players';
        $responsePlayer = $clientPlayerOG->get($urlPlayerOG);
        $OGPlayer = json_decode($responsePlayer->getBody()->getContents());
        $clientTeamOG = new Client();
        $urlTeamOG = 'https://api.opendota.com/api/teams/2586976';
        $responseTeam = $clientTeamOG->get($urlTeamOG);
        $OGTeam = json_decode($responseTeam->getBody()->getContents());

        //Liquid
        $clientPlayerLiquid = new Client();
        $urlPlayerLiquid = 'https://api.opendota.com/api/teams/2163/players';
        $responsePlayer = $clientPlayerLiquid->get($urlPlayerLiquid);
        $LiquidPlayer = json_decode($responsePlayer->getBody()->getContents());
        $clientTeamLiquid = new Client();
        $urlTeamLiquid = 'https://api.opendota.com/api/teams/2163';
        $responseTeam = $clientTeamLiquid->get($urlTeamLiquid);
        $LiquidTeam = json_decode($responseTeam->getBody()->getContents());

        return view('dota.team', compact('secretPlayer','secretTeam','OGPlayer','OGTeam','LiquidPlayer', 'LiquidTeam'));
    }

    public function heros(){
        $clientHeros = new Client();
        $urlHero = 'https://api.opendota.com/api/heroStats';
        $responseHero = $clientHeros->get($urlHero);
        $Heros = json_decode($responseHero->getBody()->getContents());
        // dd($Heros);
        return view('dota.hero', compact('Heros'));
    }
}
