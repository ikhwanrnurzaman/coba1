<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class FortniteController extends Controller
{
    //

    // public function index($username)
    public function showfortnite(){
        return view('fortnite.showfortnite');
    }
    


    public function allitem()
    {
        $token = env('GITLAB_TOKEN');
        $username = env('USER_NAME');
        $client = new Client();
        $uri = "https://fortnite-public-api.theapinetwork.com/prod09/items/list";
        $header = array('headers' => array('access_token' =>  $token));
        $response = $client->get($uri, $header);
        $projectLists = json_decode($response->getBody()->getContents());
        return view('fortnite.allItem', compact('projectLists'));
    }

    public function weapon()
    {
        $token = env('GITLAB_TOKEN');
        $username = env('USER_NAME');
        $client = new Client();
        $uri = "https://fortnite-public-api.theapinetwork.com/prod09/weapons/get";
        $header = array('headers' => array('access_token' =>  $token));
        $response = $client->get($uri, $header);
        $projectLists = json_decode($response->getBody()->getContents());
        $weapons = $projectLists->weapons;
        //dd($weapons);
        return view('fortnite.Weapon', compact('weapons'));
    }
   

   public function news()
   {
    $token = env('GITLAB_TOKEN');
    $username = env('USER_NAME');
    $client = new Client();
    $uri = "https://fortnite-public-api.theapinetwork.com/prod09/br_motd/get?";
    $header = array('headers' => array('access_token' =>  $token));
    $response = $client->get($uri, $header);
    $projectLists = json_decode($response->getBody()->getContents());
    $entries = $projectLists->entries;
    return view('fortnite.news', compact('entries')); 
   }
}
